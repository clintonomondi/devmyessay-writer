$('#submitbtn').attr("disabled",true);  //Initially disabled button when document loaded.
$('#check').click(function(){
    $('#submitbtn').attr("disabled",!$(this).is(":checked"));
});

function  showtimeTime(){
    var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = date+' '+time;
    // document.getElementById('datefd').value=dateTime;
    return dateTime;
}

function clearSession() {
    localStorage.removeItem('token');
    localStorage.removeItem('auth');
}
