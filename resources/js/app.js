import Vue from 'vue'
import VueRouter from 'vue-router'
import VueAxios from 'vue-axios';
import axios from 'axios';
import VueTelInput from 'vue-tel-input';
import Notifications from 'vue-notification';
import vueCountryRegionSelect from 'vue-country-region-select';
import JsonExcel from "vue-json-excel";
import moment from 'moment'
import VueGeolocation from 'vue-browser-geolocation';
import JwPagination from 'jw-vue-pagination';
import VueTimeago from 'vue-timeago'

import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-sugar.css';

Vue.use(VueToast);
Vue.use(VueRouter);
Vue.use(VueAxios, axios);
Vue.use(VueTelInput);
Vue.use(Notifications);
Vue.use(vueCountryRegionSelect);
Vue.component("downloadExcel", JsonExcel);
Vue.use(VueGeolocation);
Vue.component('jw-pagination', JwPagination);
Vue.use(VueTimeago, {
    name: 'Timeago', // Component name, `Timeago` by default
    locale: 'en', // Default locale
    // We use `date-fns` under the hood
    // So you can use all locales from it
    // locales: {
    //     'zh-CN': require('date-fns/locale/zh_cn'),
    //     ja: require('date-fns/locale/ja')
    // }
});

import App from './view/App'
import Login from './pages/login'
import Home from './pages/home'
import Pending from './orders/pending'
import Detail from './orders/view'
import Profile from './pages/profile'
import Inprogress from './orders/inprogress'
import Onhold from './orders/onhold'
import Revised from './orders/revised'
import Completed from './orders/completed'
import Forget from './pages/forget_password'
import Confirm_Pass from './pages/confirm'
import Reset_Pass from './pages/reset_password'

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'login',
            component: Login
        },
        {
            path: '/home',
            name: 'home',
            component: Home
        },
        {
            path: '/pending',
            name: 'pending',
            component: Pending
        },
        {
            path: '/inprogress',
            name: 'inprogress',
            component: Inprogress
        },
        {
            path: '/onhold',
            name: 'onhold',
            component: Onhold
        },
        {
            path: '/revised',
            name: 'revised',
            component: Revised
        },
        {
            path: '/completed',
            name: 'completed',
            component: Completed
        },
        {
            path: '/detail/:id',
            name: 'detail',
            component: Detail
        },
        {
            path: '/profile',
            name: 'profile',
            component: Profile
        },
        {
            path: '/forget',
            name: 'forget',
            component: Forget
        },
        {
            path: '/confirm_pass',
            name: 'confirm_pass',
            component: Confirm_Pass
        },
        {
            path: '/reset_pass',
            name: 'reset_pass',
            component: Reset_Pass
        },

    ],
});


const app = new Vue({
    el: '#app',
    components: { App },
    router,

});

